use criterion::{criterion_group, criterion_main, Criterion};
use openmw_cfg::*;

fn criterion_benchmark(c: &mut Criterion) {
    let ini = get_config().unwrap();
    c.bench_function("get_plugins", |b| {
        b.iter(|| {
            get_plugins(&ini).unwrap();
        })
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
