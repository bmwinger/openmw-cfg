# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.1]

### Fixed
- The OpenCS data directory is now included in the VFS 

## [0.5.0]

### Changed
- Dual licensed project as MIT or GPL-3+
- Sped up VFS lookups via parallelization with rayon

## [0.4.0]

### Added
- Support for finding and extracting files from BSA archives in the VFS

## [0.3.0] - 2022-09-13

### Dependencies
- Added dependency on shellexpand 2.1.

### Added
- Support for and tilde expansion in the OPENMW_CONFIG/OPENMW_CONFIG_DIR variables

## [0.2.0] - 2022-09-10

### Dependencies
- Supports `rust-ini` version `0.18` and `dirs` version `4`

### Added
- Support for specifying the config directory using `OPENMW_CONFIG_DIR`.

## [0.1.3] - 2021-05-26

### Bugfixes
- Fixed config path on macOS to be in ~/Library/Preferences
