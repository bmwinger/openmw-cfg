# OpenMW cfg

This library provides tools for accessing the case insensitive virtual file system defined by OpenMW in `openmw.cfg`.

The location of the `openmw.cfg` file is determined by the location described in the [OpenMW paths documentation](https://openmw.readthedocs.io/en/stable/reference/modding/paths.html), and can be overridden by end users using the environment variable `OPENMW_CONFIG_DIR` or `OPENMW_CONFIG`, to allow for systems that use a non-standard location.

`OPENMW_CONFIG_DIR` can contain multiple paths, separated by colons (or semicolons on Windows), and these paths will be searched in turn to find one which contains a file with the name `openmw.cfg`.
