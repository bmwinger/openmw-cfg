extern crate dirs;
extern crate ini;

mod config;

#[cfg(feature = "bsa")]
pub use config::find_file_or_archive;
pub use config::{
    config_path, find_file, get_archives, get_config, get_data_dirs, get_encoding, get_plugins,
    read_file, Error, Ini, VFSFile,
};
