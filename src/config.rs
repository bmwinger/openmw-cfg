pub use ini::Ini;
use rayon::prelude::*;
use std::env::var;
use std::fs;
use std::path::{Component, Path, PathBuf};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Error parsing openmw.cfg: {0}")]
    IniError(ini::Error),
    #[error("FileNotFound: {0}")]
    FileNotFound(PathBuf),
    #[error("ci_exists cannot take an aboslute path as an argument")]
    AbsolutePathError,
    #[error("Encoding was missing from openmw.cfg")]
    MissingEncoding,
    #[error("IOError: {0}")]
    IOError(std::io::Error),
    #[error("Error reading BSA file: {0}")]
    #[cfg(feature = "bsa")]
    BSAError(bsatoollib::error::BsaError),
}

impl From<ini::Error> for Error {
    fn from(error: ini::Error) -> Self {
        Self::IniError(error)
    }
}

#[cfg(feature = "bsa")]
impl From<bsatoollib::error::BsaError> for Error {
    fn from(error: bsatoollib::error::BsaError) -> Self {
        Self::BSAError(error)
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Self::IOError(error)
    }
}

/// Returns the path of openmw.cfg
///
/// Defaults:
/// - Linux: ~/.config/openmw/openmw.cfg
/// - macOS: ~/Library/Preferences/openmw/openmw.cfg
/// - Windows: %Documents%/My Games/openmw/openmw.cfg
///
/// The OPENMW_CONFIG environment variable can be used to
/// override this location.
pub fn config_path() -> PathBuf {
    // Allow OPENMW_CONFIG env var to override default
    if let Ok(path) = var("OPENMW_CONFIG") {
        return shellexpand::tilde(&path).into_owned().into();
    } else if let Ok(path) = var("OPENMW_CONFIG_DIR") {
        let paths = if cfg!(windows) {
            path.split(';')
        } else {
            path.split(':')
        };
        for path in paths {
            let mut config_path: PathBuf = shellexpand::tilde(&path).into_owned().into();
            config_path = config_path.join("openmw.cfg");
            if config_path.exists() {
                return config_path;
            }
        }
    }

    if cfg!(windows) {
        dirs::document_dir()
            .unwrap()
            .join("My Games")
            .join("openmw")
            .join("openmw.cfg")
    } else {
        // Same as config_dir, but ~/Library/Preferences on MacOS
        dirs::preference_dir()
            .unwrap()
            .join("openmw")
            .join("openmw.cfg")
    }
}

/// Returns an Ini object for openmw.cfg
///
/// The result is intended to be passed to other functions,
/// rather than used directly.
pub fn get_config() -> Result<Ini, Error> {
    let path = config_path();
    let conf = Ini::load_from_file_noescape(path)?;
    Ok(conf)
}

/// Returns the data directories listed in openmw.cfg
pub fn get_data_dirs(conf: &Ini) -> Result<Vec<String>, Error> {
    let openmw_data = if cfg!(windows) {
        dirs::document_dir()
            .unwrap()
            .join("My Games")
            .join("openmw")
            .join("data")
    } else {
        dirs::data_dir().unwrap().join("openmw").join("data")
    };
    let section = conf
        .section::<String>(None)
        .expect("Default section is not present!");

    let mut dirs: Vec<String> = section.get_all("data").map(|x| x.to_string()).collect();
    dirs.push(openmw_data.display().to_string());
    Ok(dirs)
}

/// Returns the absolute paths of the plugins listed in openmw.cfg
pub fn get_plugins(conf: &Ini) -> Result<Vec<PathBuf>, Error> {
    let section = conf
        .section::<String>(None)
        .expect("Default section is not present!");

    let plugin_names: Vec<&str> = section.get_all("content").collect();
    let paths: Result<Vec<PathBuf>, Error> = plugin_names
        .into_par_iter()
        .map(|plugin_name| find_file(conf, plugin_name))
        .collect();
    paths
}

/// Returns the absolute paths of the fallback-archives listed in openmw.cfg
pub fn get_archives(conf: &Ini) -> Result<Vec<PathBuf>, Error> {
    let section = conf
        .section::<String>(None)
        .expect("Default section is not present!");

    let archive_names: Vec<&str> = section.get_all("fallback-archive").collect();

    let paths: Result<Vec<PathBuf>, Error> = archive_names
        .into_par_iter()
        .map(|name| find_file(conf, name))
        .collect();
    paths
}

/// Case sensitive existence function.
/// Returns the (case-insensitive) path of the file if it exists.
pub fn ci_exists(root: &Path, path: &Path) -> Result<PathBuf, Error> {
    let possible_path = root.join(path);
    if possible_path.exists() {
        return Ok(possible_path);
    }
    let mut partial_path = root.to_path_buf().canonicalize()?;

    for component in path.components() {
        match component {
            Component::Normal(component) => {
                let mut direntries = fs::read_dir(&partial_path)?;
                let result = direntries.find(|entry| match entry {
                    Ok(entry) => {
                        if entry.file_name().to_str().unwrap().to_lowercase()
                            == component.to_str().unwrap().to_lowercase()
                        {
                            partial_path = partial_path.join(entry.file_name());
                            true
                        } else {
                            false
                        }
                    }
                    Err(error) => {
                        eprintln!("Warning: Could not read file {}", error);
                        false
                    }
                });
                if result.is_none() {
                    return Err(Error::FileNotFound(partial_path));
                }
            }
            Component::CurDir => (),
            Component::ParentDir => {
                partial_path.pop();
            }
            Component::RootDir | Component::Prefix(_) => return Err(Error::AbsolutePathError),
        }
    }

    if partial_path.exists() {
        Ok(partial_path)
    } else {
        Err(Error::FileNotFound(root.join(path)))
    }
}

pub enum VFSFile {
    File(PathBuf),
    ArchivedFile(PathBuf),
}

/// Searches the openmw vfs for the given file
/// Returns the absolute path of the file within the VFS
pub fn find_file(ini: &Ini, filename: &str) -> Result<PathBuf, Error> {
    let dirs = get_data_dirs(ini)?;
    let result = dirs.into_par_iter().rev().find_map_first(|dir| {
        if let Ok(path) = ci_exists(Path::new(&dir), Path::new(filename)) {
            Some(path)
        } else {
            None
        }
    });
    if let Some(result) = result {
        return Ok(result);
    }
    Err(Error::FileNotFound(Path::new(filename).to_path_buf()))
}

fn normalize(path: &str) -> String {
    path.to_lowercase().replace('\\', "/")
}

/// Searches the openmw vfs for the given file
/// Returns the absolute path of the file within the VFS
/// If the file is within an archive, returns the path of the archive
#[cfg(feature = "bsa")]
pub fn find_file_or_archive(ini: &Ini, filename: &str) -> Result<VFSFile, Error> {
    if let Ok(path) = find_file(ini, filename) {
        return Ok(VFSFile::File(path));
    }
    for archive_path in get_archives(ini)?.into_iter().rev() {
        let mut bsa = bsatoollib::BSAFile::default();
        let path_name = archive_path.display().to_string();
        bsa.open(&path_name)?;
        for file in bsa.get_list() {
            if normalize(&file.name) == normalize(filename) {
                return Ok(VFSFile::ArchivedFile(archive_path));
            }
        }
    }
    Err(Error::FileNotFound(Path::new(filename).to_path_buf()))
}

/// Searches the openmw vfs for the given file and returns its contents
pub fn read_file(ini: &Ini, filename: &str) -> Result<Vec<u8>, Error> {
    let dirs = get_data_dirs(ini)?;
    let dirs = dirs.into_iter().rev();
    for dir in dirs {
        if let Ok(path) = ci_exists(Path::new(&dir), Path::new(filename)) {
            return Ok(std::fs::read(path)?);
        }
    }
    #[cfg(feature = "bsa")]
    for archive_path in get_archives(ini)?.iter().rev() {
        let mut bsa = bsatoollib::BSAFile::default();
        let path_name = archive_path.display().to_string();
        bsa.open(&path_name)?;
        let mut result = None;
        for file in bsa.get_list() {
            if normalize(&file.name) == normalize(filename) {
                result = Some(bsa.get_file(&file.name));
            }
        }
        match result {
            Some(Ok(data)) => return Ok(data),
            Some(Err(bsatoollib::error::BsaError::FileNotFound(_))) | None => (),
            Some(Err(x)) => Err(x)?,
        }
    }
    Err(Error::FileNotFound(Path::new(filename).to_path_buf()))
}

/// Fetches encoding from openmw.cfg
pub fn get_encoding() -> Result<String, Error> {
    let path = config_path();
    let conf = Ini::load_from_file_noescape(path)?;
    let section = conf
        .section::<String>(None)
        .expect("Default section is not present!");

    section
        .get("encoding")
        .map(|x| x.to_string())
        .ok_or(Error::MissingEncoding)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_plugins() {
        let ini = get_config().unwrap();
        get_plugins(&ini).unwrap();
    }
}
